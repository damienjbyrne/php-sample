<?php

/**
 * Decimal Framework
 *
 * LICENSE
 *
 * Please do not distribute this software.
 *
 * @category    Decimal
 * @package     Decimal_Chart
 * @copyright   Copyright (c) 2006-2015 Decimal Pty Ltd. (http://www.decimal.com.au)
 * @license     http://www.decimal.com.au    Proprietary. Patents Pending.
 * @version     $Id$
 */

/**
 * Decimal Charting Component
 *
 * @category    Decimal
 * @package     Decimal_Chart
 * @author      Rob Morgan <rm@decimal.com.au>
 * @author      Damien Byrne <db@decimal.com>
 * @copyright   Copyright (c) 2006-2015 Decimal Pty Ltd. (http://www.decimal.com.au)
 * @license     http://www.decimal.com.au    Proprietary. Patents Pending.
 */
class Decimal_Chart {
    // define chart types
    const LINE = "line";
    const BAR  = "bar"; // Note, the previous implementation of Coldfusion calls vertical column charts as bar, as does the rest of the reporting code.  This convention is being kept for now.
    const PIE  = "pie";
    const STEP = "step";
    //const AREA = "area";

    /** @var Decimal_Chart_Adapter_Interface Chart Adapter */
    protected $adapter;

    /** @var array Chart Properties */
    protected $properties;

    /** @var array Chart Categories */
    protected $categories;

    /** @var array Chart Datasets */
    protected $datasets;

    /** @var  string name of theme to apply to chart */
    protected $themeName;

    /** @var  string URL to find the chart theme data */
    protected $themeUrl;

    /** @var string Chart Type */
    protected $type;

    /** @var bool is the chart animated */
    protected $animated = true;

    /** @var int Chart Width */
    protected $width;

    /** @var int Chart Height */
    protected $height;

    /** @var bool Show the chart legend */
    protected $legendVisible = true;

    /** @var  bool Is the Chart 3D */
    protected $threeD = false;

    /** @var string Chart Title */
    protected $title;

    /** @var string Chart SubTitle */
    protected $subTitle;

    /** @var string Chart title for x axis */
    protected $xAxisTitle;

    /** @var  int number of steps before another data label is printed. */
    protected $xAxisLabelStep;

    /** @var string Chart Chart title for primary y axis */
    protected $yAxisTitlePrimary;

    /** @var  string Description of the format type - to be translated by the adapter */
    protected $yAxisFormat;

    /** @var string Chart title for secondary y axis */
    protected $yAxisTitleSecondary;

    /** @var  string Description of the format type - to be translated by the adapter */
    protected $yAxisFormatSecondary;

    /** @var  bool for a pie chart, are the slices separated */
    protected $exploded = false;

    /** @var bool for a bar chart, are the series stacked */
    protected $stacked = false;

    /**
     * Class constructor.
     *
     * Create a new Chart.
     *
     * @param Decimal_Chart_Adapter_Interface|null $adapter to render charts
     */
    public function __construct(Decimal_Chart_Adapter_Interface $adapter = null) {
        if ($adapter !== null) {
            $this->setAdapter($adapter);
        }
    }

    /**
     * Set the Chart Rendering Adapter
     *
     * @param $adapter Decimal_Chart_Adapter_Interface
     * @return Decimal_Chart
     */
    public function setAdapter(Decimal_Chart_Adapter_Interface $adapter) {
        $this->adapter = $adapter;
        $this->adapter->setChart($this);

        return $this;
    }

    /**
     * @return string
     */
    public function getThemeName() {
        return $this->themeName;
    }

    /**
     * @param string $themeName
     * @return Decimal_Chart
     */
    public function setThemeName($themeName) {
        $this->themeName = $themeName;

        return $this;
    }

    /**
     * @param string $themeUrl
     * @return Decimal_Chart
     */
    public function setThemeUrl($themeUrl) {
        $this->themeUrl = $themeUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getThemeUrl() {
        return $this->themeUrl;
    }

    /**
     * @return boolean
     */
    public function isAnimated() {
        return $this->animated;
    }

    /**
     * @param boolean $animated
     * @return Decimal_Chart
     */
    public function setAnimated($animated) {
        $this->animated = $animated;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isThreeD() {
        return $this->threeD;
    }

    /**
     * @param boolean $threeD
     * @return Decimal_Chart
     */
    public function setThreeD($threeD) {
        $this->threeD = $threeD;

        return $this;
    }

    /**
     * @return string
     */
    public function getHeight() {
        return $this->height;
    }

    /**
     * @param int $height
     * @return Decimal_Chart
     */
    public function setHeight($height) {
        $this->height = $height;

        return $this;
    }

    /**
     * @return string
     */
    public function getWidth() {
        return $this->width;
    }

    /**
     * @param int $width
     * @return Decimal_Chart
     */
    public function setWidth($width) {
        $this->width = $width;

        return $this;
    }

    /**
     * @param string $type
     * @return Decimal_Chart
     */
    public function setType($type) {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * @return boolean
     */
    public function isLegendVisible() {
        return $this->legendVisible;
    }

    /**
     * @param boolean $legendVisible
     * @return Decimal_Chart
     */
    public function setLegendVisible($legendVisible) {
        $this->legendVisible = $legendVisible;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Decimal_Chart
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubTitle() {
        return $this->subTitle;
    }

    /**
     * @param string $subTitle
     * @return Decimal_Chart
     */
    public function setSubTitle($subTitle) {
        $this->subTitle = $subTitle;

        return $this;
    }

    /**
     * @return string
     */
    public function getXAxisTitle() {
        return $this->xAxisTitle;
    }

    /**
     * @param string $xAxisTitle
     * @return Decimal_Chart
     */
    public function setXAxisTitle($xAxisTitle) {
        $this->xAxisTitle = $xAxisTitle;

        return $this;
    }

    /**
     * @return int
     */
    public function getXAxisLabelStep() {
        return $this->xAxisLabelStep;
    }

    /**
     * @param int $xAxisLabelStep
     * @return Decimal_Chart
     */
    public function setXAxisLabelStep($xAxisLabelStep) {
        $this->xAxisLabelStep = $xAxisLabelStep;

        return $this;
    }

    /**
     * @return string
     */
    public function getYAxisFormat() {
        return $this->yAxisFormat;
    }

    /**
     * @param string $yAxisFormat
     * @return Decimal_Chart
     */
    public function setYAxisFormat($yAxisFormat) {
        $this->yAxisFormat = $yAxisFormat;

        return $this;
    }

    /**
     * @return string
     */
    public function getYAxisFormatSecondary() {
        return $this->yAxisFormatSecondary;
    }

    /**
     * @param string $yAxisFormatSecondary
     * @return Decimal_Chart
     */
    public function setYAxisFormatSecondary($yAxisFormatSecondary) {
        $this->yAxisFormatSecondary = $yAxisFormatSecondary;

        return $this;
    }

    /**
     * @return string
     */
    public function getYAxisTitlePrimary() {
        return $this->yAxisTitlePrimary;
    }

    /**
     * @param string $yAxisTitlePrimary
     * @return Decimal_Chart
     */
    public function setYAxisTitlePrimary($yAxisTitlePrimary) {
        $this->yAxisTitlePrimary = $yAxisTitlePrimary;

        return $this;
    }

    /**
     * @return string
     */
    public function getYAxisTitleSecondary() {
        return $this->yAxisTitleSecondary;
    }

    /**
     * @param string $yAxisTitleSecondary
     * @return Decimal_Chart
     */
    public function setYAxisTitleSecondary($yAxisTitleSecondary) {
        $this->yAxisTitleSecondary = $yAxisTitleSecondary;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isExploded() {
        return $this->exploded;
    }

    /**
     * @param boolean $exploded
     * @return Decimal_Chart
     */
    public function setExploded($exploded) {
        $this->exploded = $exploded;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isStacked() {
        return $this->stacked;
    }

    /**
     * @param boolean $stacked
     * @return Decimal_Chart
     */
    public function setStacked($stacked) {
        $this->stacked = $stacked;

        return $this;
    }


    /**
     * Set the Chart Properties
     *
     * @param array $properties
     * @return Decimal_Chart
     */
    public function setProperties($properties) {
        $this->properties = $properties;

        return $this;
    }

    /**
     * Get the Chart Properties
     *
     * @return array
     */
    public function getProperties() {
        return $this->properties;
    }

    /**
     * Add a Chart Category
     *
     * @param string $label
     * @return void
     */
    public function addCategory($label) {
        $this->categories[] = $label;
    }

    /**
     * Get the Chart Categories
     *
     * @return array
     */
    public function getCategories() {
        return $this->categories;
    }

    /**
     * Add a Chart Dataset
     *
     * @param Decimal_Chart_Dataset $dataset
     * @return Decimal_Chart
     */
    public function addDataset(Decimal_Chart_Dataset $dataset) {
        $this->datasets[] = $dataset;

        return $this;
    }

    /**
     * Get the Chart Datasets
     *
     * @return array of Decimal_Chart_Dataset
     */
    public function getDatasets() {
        return $this->datasets;
    }

    /**
     * Render the Chart using the Specified Adapter
     *
     * @return string
     * @throws Decimal_Chart_Exception
     */
    public function render() {
        if (!$this->adapter) {
            throw new Decimal_Chart_Exception('Cannot Render Chart without a Valid Chart Adapter set');
        }

        return $this->adapter->render($this);
    }

    /**
     * @return string
     * @throws Decimal_Chart_Exception
     */
    public function getChartJavascriptFunction() {
        return $this->adapter->getChartJavascriptFunction();
    }

    /**
     * @return string
     * @throws Decimal_Chart_Exception
     */
    public function getJavascriptChartLibrary() {
        return $this->adapter->getJavascriptChartLibrary();
    }

    /**
     * @return string
     * @throws Decimal_Chart_Exception
     */
    public function getThemeContents() {

        return $this->adapter->getChartTheme();
    }
}
