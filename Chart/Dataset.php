<?php

/**
 * Decimal Framework
 *
 * LICENSE
 *
 * Please do not distribute this software.
 *
 * @category    Decimal
 * @package     Decimal_Chart
 * @copyright   Copyright (c) 2006-2015 Decimal Pty Ltd. (http://www.decimal.com.au)
 * @license     http://www.decimal.com.au    Proprietary. Patents Pending.
 * @version     $Id$
 */

/**
 * Decimal Chart Dataset
 *
 * @category    Decimal
 * @package     Decimal_Chart
 * @author      Rob Morgan <rm@decimal.com.au>
 * @author      Damien Byrne <db@decimal.com>
 * @copyright   Copyright (c) 2006-2015 Decimal Pty Ltd. (http://www.decimal.com.au)
 * @license     http://www.decimal.com.au    Proprietary. Patents Pending.
 */
class Decimal_Chart_Dataset {
    /** @var array The Dataset Properties */
    protected $properties;

    /** @var array The Data to Represent */
    protected $data;

    /** @var  string name of the dataset (used as title) */
    protected $name;

    /** @var  string type of series plot (for multi style charts, i.e. bar plus line) */
    protected $type;

    /** @var  string sets a custom colour attribute for this dataset, in #nnnnnn format */
    protected $colour;

    /** @var bool does the set use the secondary Y axis */
    protected $secondaryYAxis = false;

    /**
     * Class constructor
     *
     * @param array|null $data
     */
    public function __construct($data = null) {
        $this->data = $data;
    }

    /**
     * Set the Dataset Data
     *
     * @param array $data
     * @return void
     */
    public function setData($data) {
        $this->data = $data;
    }

    /**
     * Get the Dataset Data
     *
     * @return array
     */
    public function getData() {
        return $this->data;
    }

    /**
     * Set the Dataset Properties
     *
     * @param array $properties
     * @return void
     */
    public function setProperties($properties) {
        $this->properties = $properties;
    }

    /**
     * Get the Dataset Properties
     *
     * @return array
     */
    public function getProperties() {
        return $this->properties;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Decimal_Chart_Dataset
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Decimal_Chart_Dataset
     */
    public function setType($type) {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getColour() {
        return $this->colour;
    }

    /**
     * @param string $colour
     * @return Decimal_Chart_Dataset
     */
    public function setColour($colour) {
        $this->colour = $colour;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isSecondaryYAxis() {
        return $this->secondaryYAxis;
    }

    /**
     * @param boolean $secondaryYAxis
     * @return Decimal_Chart_Dataset
     */
    public function setSecondaryYAxis($secondaryYAxis) {
        $this->secondaryYAxis = $secondaryYAxis;

        return $this;
    }


}
