<?php

/**
 * Decimal Framework
 *
 * LICENSE
 *
 * Please do not distribute this software.
 *
 * @category    Decimal
 * @package     Decimal_Chart
 * @copyright   Copyright (c) 2006-2015 Decimal Pty Ltd. (http://www.decimal.com.au)
 * @license     http://www.decimal.com.au    Proprietary. Patents Pending.
 * @version     $Id$
 */

/**
 * Creates FusionCharts JSON
 * For more information on FusionCharts see http://www.fusioncharts.com.
 *
 * @category    Decimal
 * @package     Decimal_Chart
 * @subpackage  Adapter
 * @author      Damien Byrne <db@decimal.com>
 * @copyright   Copyright (c) 2015 Decimal Pty Ltd. (http://www.decimal.com)
 * @license     http://www.decimal.com    Proprietary. Patents Pending.
 */
class Decimal_Chart_Adapter_FusionChartType {

    public $hasLine       = false;
    public $hasBar        = false;
    public $hasArea       = false;
    public $hasPie        = false;
    public $isThreeD      = false;
    public $isStacked     = false;
    public $isMultiSeries = false;
    public $hasDualYAxis  = false;

    CONST LINE    = 0b00000001;
    CONST BAR     = 0b00000010;
    CONST AREA    = 0b00000100;
    CONST PIE     = 0b00001000;
    CONST THREED  = 0b00010000;
    CONST STACKED = 0b00100000;
    CONST MULTI   = 0b01000000;
    CONST DUALY   = 0x10000000;

    /** @var array Array of the attributes bit flagged with the corresponding type */
    private $typeMatrix = [];

    public function __construct() {
        $this->typeMatrix = [
            self::PIE                                                           => "pie2d",
            self::PIE | self::THREED                                            => "pie3d",
            self::LINE                                                          => "line",
            self::LINE | self::THREED                                           => "line", // fusion charts doesn't seem to have a 3d line chart
            self::LINE | self::MULTI                                            => "msline",
            self::BAR                                                           => "column2d",
            self::BAR | self::STACKED                                           => "stackedcolumn2d",
            self::BAR | self::MULTI                                             => "mscolumn2d",
            self::BAR | self::STACKED | self::MULTI                             => "stackedcolumn2d", // we are not doing true multi series stacked that I can tell. Just use the stacked chart.
            self::BAR | self::THREED                                            => "column3d",
            self::BAR | self::MULTI | self::THREED                              => "mscolumn3d",
            self::BAR | self::STACKED | self::THREED                            => "stackedcolumn3d",
            self::BAR | self::STACKED | self::MULTI | self::THREED              => "stackedcolumn3d",
            self::BAR | self::LINE                                              => "mscombi2d",
            self::BAR | self::MULTI | self::LINE                                => "mscombi2d",
            self::BAR | self::LINE | self::DUALY                                => "mscombidy2d",
            self::BAR | self::MULTI | self::LINE | self::DUALY                  => "mscombidy2d",
            self::BAR | self::STACKED | self::LINE                              => "stackedcolumn2dline",
            self::BAR | self::STACKED | self::MULTI | self::LINE                => "stackedcolumn2dline",
            self::BAR | self::LINE | self::THREED                               => "mscombi3d",
            self::BAR | self::MULTI | self::LINE | self::THREED                 => "mscombi3d",
            self::BAR | self::STACKED | self::LINE | self::THREED               => "stackedcolumn3dline",
            self::BAR | self::STACKED | self::LINE | self::THREED | self::DUALY => "stackedcolumn3dlinedy",

            self::AREA                                                          => "area2d", // I don't think we currently use area graphs, add them if you need them
        ];

    }

    /**
     * @return string
     * @throws Decimal_Chart_Exception
     */
    public function getFusionChartType() {
        $typeKey = 0;
        $typeKey |= $this->hasLine ? self::LINE : 0;
        $typeKey |= $this->hasBar ? self::BAR : 0;
        $typeKey |= $this->hasArea ? self::AREA : 0;
        $typeKey |= $this->hasPie ? self::PIE : 0;
        $typeKey |= $this->isThreeD ? self::THREED : 0;
        $typeKey |= $this->isStacked ? self::STACKED : 0;
        $typeKey |= $this->isMultiSeries ? self::MULTI : 0;
        $typeKey |= $this->hasDualYAxis ? self::DUALY : 0;

        if (array_key_exists($typeKey, $this->typeMatrix)) {
            return (string) $this->typeMatrix[$typeKey];
        } else {
            $attributes = "";
            $attributes .= $this->hasLine ? "line " : "";
            $attributes .= $this->hasBar ? "bar " : "";
            $attributes .= $this->hasArea ? "area " : "";
            $attributes .= $this->hasPie ? "pie " : "";
            $attributes .= $this->isThreeD ? "3d " : "";
            $attributes .= $this->isStacked ? "stacked " : "";
            $attributes .= $this->isMultiSeries ? "multi " : "";
            $attributes .= $this->hasDualYAxis ? "dualYAxis " : "";

            throw new Decimal_Chart_Exception("Cannot determine chart type for this combination ( attributes: $attributes)");
        }
    }
}
