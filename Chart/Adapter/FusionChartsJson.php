<?php

/**
 * Decimal Framework
 *
 * LICENSE
 *
 * Please do not distribute this software.
 *
 * @category    Decimal
 * @package     Decimal_Chart
 * @copyright   Copyright (c) 2006-2015 Decimal Pty Ltd. (http://www.decimal.com.au)
 * @license     http://www.decimal.com.au    Proprietary. Patents Pending.
 * @version     $Id$
 */

/**
 * Creates FusionCharts JSON
 * For more information on FusionCharts see http://www.fusioncharts.com.
 *
 * @category    Decimal
 * @package     Decimal_Chart
 * @subpackage  Adapter
 * @author      Rob Morgan <rm@decimal.com.au>
 * @author      Damien Byrne <db@decimal.com>
 * @copyright   Copyright (c) 2006-2015 Decimal Pty Ltd. (http://www.decimal.com)
 * @license     http://www.decimal.com    Proprietary. Patents Pending.
 */
class Decimal_Chart_Adapter_FusionChartsJson implements Decimal_Chart_Adapter_Interface {
    /**
     * @var Decimal_Chart
     */
    protected $chart;

    /**
     * The Chart JSON Document
     *
     * @var string
     */
    protected $json;

    /** @var  Zend_Config */
    protected $config;

    /**
     * @param Decimal_Chart|null $chart
     */
    public function __construct(Decimal_Chart $chart = null) {
        if (isset($chart)) {
            $this->chart = $chart;
        }

        $this->config = \Zend_Registry::get('config');
    }

    /**
     * @param Decimal_Chart|null $chart
     * @return string
     */
    public function render(Decimal_Chart $chart = null) {
        return $this->generateChartData();
    }

    /**
     * Render a chart instance to the FusionCharts JSON description.
     *
     * @param Decimal_Chart|null $chart to be rendered
     * @return string JSON
     * @throws Decimal_Chart_Exception
     */
    public function generateChartData() {
        if (empty($this->json)) { // The JSON was not generated yet
            if (empty($this->chart)) {
                throw new Decimal_Chart_Exception("Cannot generate chart data, no chart set");
            }

            $chartData          = [];
            $chartData["chart"] = $this->getChartProperties();

            $datasets = $this->chart->getDatasets();
            // use categories if we are multi series
            if (count($datasets) > 1) {
                $chartData["categories"]             = [];
                $chartData["categories"]["category"] = [];
                $categories                          = $this->chart->getCategories();
                if (null !== $categories) {
                    // Create Categories
                    foreach ($categories as $categoryKey) {
                        $chartData["categories"]["category"][] = ["label" => $categoryKey];
                    }
                } else {
                    // multi series needs categories, make them
                    foreach ($datasets[0]->getData() as $value) {
                        if (is_array($value) && array_key_exists("value", $value) && array_key_exists("label", $value)) {
                            $chartData["categories"]["category"][] = ["label" => $value["label"]];
                        } else { // Create dummy categories
                            $chartData["categories"]["category"][] = ["label" => ""];
                        }
                    }
                }
            }

            // Create Datasets
            $chartData["dataset"] = [];

            $first=true;
            /** @var Decimal_Chart_Dataset $dataset */
            foreach ($datasets as $dataset) {
                $datasetJson = [];
                // Add in Dataset Properties
                $datasetProperties = $dataset->getProperties();
                if (is_array($datasetProperties)) {
                    $datasetJson = $datasetProperties;
                }

                $datasetName = $dataset->getName();
                if ($datasetName) {
                    $datasetJson["seriesname"] = $datasetName;
                }

                $datasetType = $dataset->getType();
                if ($datasetType) {
                    $datasetJson["renderAs"] = $datasetType;
                }

                $datasetColour = $dataset->getColour();
                if ($datasetColour) {
                    $datasetJson["color"] = $datasetColour;
                }

                if ($dataset->isSecondaryYAxis()) {
                    $datasetJson["parentYAxis"] = "S";
                }

                // Add Dataset Values
                $datasetValues       = $dataset->getData();
                $datasetJson["data"] = [];
                foreach ($datasetValues as $element) {
                    if (is_array($element) && array_key_exists("value", $element) && array_key_exists("label", $element)) {
                        $value=$this->nullValueIfRequired($element['value'],$first);
                        $label=$element['label'];
                        $datasetJson["data"][] = ['label'=>$label, 'value' => $value];
                    } else { // plain data array
                        $value=$this->nullValueIfRequired($element,$first);
                        $datasetJson["data"][] = ["value" => $value];
                    }
                }

                $chartData["dataset"][] = $datasetJson;
                $first=false;
            }

            //store the build array as a JSON string
            $this->json = json_encode($chartData);
        }

        return $this->json;
    }

    protected function nullValueIfRequired($element,$first){
        $value=floatval($element);
        if ($this->chart->getType()=='bar'){
            $value=$value?$value:null;
        }
        return($value);
    }


    /**
     * @param Decimal_Chart $chart
     * @return Decimal_Chart_Adapter_FusionChartsJson
     */
    public function setChart(Decimal_Chart $chart) {
        $this->chart = $chart;

        return $this;
    }

    /**
     * @return string
     */
    public function getJson() {
        if (empty($this->json)) {
            $this->generateChartData();
        }

        return $this->json;
    }

    private function getChartProperties() {
        // Get the bulk other Chart Properties
        $chartProperties = $this->chart->getProperties();

        // Unpack the universal generic properties
        $chartTitle = $this->chart->getTitle();
        if (isset($chartTitle)) {
            $chartProperties['caption'] = $chartTitle;
        }

        $chartSubTitle = $this->chart->getSubTitle();
        if (isset($chartSubTitle)) {
            $chartProperties['subcaption'] = $chartSubTitle;
        }

        $chartXAxisTitle = $this->chart->getXAxisTitle();
        if (isset($chartXAxisTitle)) {
            $chartProperties['xAxisName'] = $chartXAxisTitle;
        }

        $chartYAxisTitleSecondary = $this->chart->getYAxisTitleSecondary();
        if (isset($chartYAxisTitleSecondary)) {
            $chartProperties['sYAxisName'] = $chartYAxisTitleSecondary;
        }

        $chartYAxisTitlePrimary = $this->chart->getYAxisTitlePrimary();
        if (isset($chartYAxisTitlePrimary)) {
            if (isset($chartYAxisTitleSecondary)) {
                $chartProperties['pYAxisName '] = $chartYAxisTitlePrimary;
            } else {
                $chartProperties['yAxisName '] = $chartYAxisTitlePrimary;
            }
        }

        $yAxisFormat = $this->chart->getYAxisFormat();
        if ($yAxisFormat !== null) {
            if (strtolower($yAxisFormat) === 'currency') {
                $chartProperties["numberPrefix"] = '$';
            } elseif (strtolower($yAxisFormat) === 'percent') {
                $chartProperties["numberSuffix"] = '%';
            }
        }

        $yAxisFormat2 = $this->chart->getYAxisFormatSecondary();
        if ($yAxisFormat2 !== null) {
            if (strtolower($yAxisFormat2) === 'currency') {
                $chartProperties["sNumberPrefix"] = '$';
            } elseif (strtolower($yAxisFormat2) === 'percent') {
                $chartProperties["sNumberSuffix"] = '%';
            }
        }

        $chartTheme = $this->chart->getThemeName();
        if (isset($chartTheme)) {
            $chartProperties['theme'] = $chartTheme;
        }

        if ($this->chart->isAnimated() === false) {
            $chartProperties["animation"] = "0";
        }

        return $chartProperties;
    }

    /**
     * @return string
     * @throws Decimal_Chart_Exception
     */
    protected function getFusionChartType() {
        // convert the Decimal Chart type to a fusion chart type
        if (empty($this->chart)) {
            throw new Decimal_Chart_Exception("Chart not set, cannot get chart type");
        }

        $fusionChartType = new Decimal_Chart_Adapter_FusionChartType();

        switch ($this->chart->getType()) {
            case Decimal_Chart::BAR: // Note: the old cold fusion implementation called vertical bar charts as just "bar".  In FusionCharts these are called column charts.
                $fusionChartType->hasBar = true;
                break;
            case Decimal_Chart::LINE:
                $fusionChartType->hasLine = true;
                break;
            case Decimal_Chart::PIE:
                $fusionChartType->hasPie = true;
                break;
            default:
                throw new Decimal_Chart_Exception("Unknown chart rendering type " . $this->chart->getType());
        }

        $datasets = $this->chart->getDatasets();

        if (count($datasets) > 1) {
            $fusionChartType->isMultiSeries = true;
        }

        $fusionChartType->isStacked = $this->chart->isStacked();
        $fusionChartType->isThreeD  = $this->chart->isThreeD();

        /** @var Decimal_Chart_Dataset $dataset */
        foreach ($datasets as $dataset) {
            $datasetType = $dataset->getType();
            if ($datasetType !== null) {
                switch (strtolower($datasetType)) {
                    case 'line':
                        $fusionChartType->hasLine = true;
                        break;
                    case 'bar':
                        $fusionChartType->hasBar = true;
                        break;
                    case 'area':
                        $fusionChartType->hasArea = true;
                        break;
                    case 'pie':
                        $fusionChartType->hasPie = true;
                        break;
                    default:
                        throw new Decimal_Chart_Exception("dataset has unknown render type");
                }
            }

            if ($dataset->isSecondaryYAxis()) {
                $fusionChartType->hasDualYAxis = true;
            }
        }

        $chartType = $fusionChartType->getFusionChartType();

        return $chartType;
        //return $fusionChart;
    }

    /**
     * @return string
     * @throws Decimal_Chart_Exception
     */
    public function getChartJavascriptFunction() {
        $chartType = $this->getFusionChartType();
        $chartData = $this->getJson();
        $width     = $this->chart->getWidth();
        $height    = $this->chart->getHeight();
        $script    = <<<JS
FusionCharts.setCurrentRenderer('javascript');
var chart = new FusionCharts("$chartType", 'chart0', "$width", "$height", "0", "1");
chart.setJSONData($chartData);
chart.render("chartContainer");
JS;

        return $script;
    }

    /**
     * @return string
     * @throws Decimal_Chart_Exception
     */
    public function getJavascriptChartLibrary() {
        // TODO: move the locations to the application.ini
        $library = file_get_contents(APPLICATION_PATH . $this->config->charts->fusioncharts->library->path . '\fusioncharts.js');
        $library .= file_get_contents(APPLICATION_PATH . $this->config->charts->fusioncharts->library->path . '\fusioncharts.charts.js');

        return $library;
    }

    /**
     * @return string
     * @throws Decimal_Chart_Exception
     */
    public function getChartTheme() {
        $themeURL  = $this->chart->getThemeUrl();
        $themeName = $this->chart->getThemeName();
        if (empty($themeURL)) {
            if (empty($themeName)) {
                return "";
            } else {
                $themeLocation = APPLICATION_PATH . $this->config->charts->fusioncharts->theme->path . "\\fusioncharts.theme.$themeName.js";
            }
        } else {
            $themeLocation = $themeURL;
        }

        if (file_exists($themeLocation)) {
            return file_get_contents($themeLocation);
        } else {
            throw new Decimal_Chart_Exception("Cannot find chart theme file: $themeName at $themeLocation");
        }
    }
}
