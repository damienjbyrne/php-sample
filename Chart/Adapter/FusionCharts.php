<?php

/**
 * Decimal Framework
 *
 * LICENSE
 *
 * Please do not distribute this software.
 *
 * @category    Decimal
 * @package     Decimal_Chart
 * @copyright   Copyright (c) 2006-2015 Decimal Pty Ltd. (http://www.decimal.com.au)
 * @license     http://www.decimal.com.au    Proprietary. Patents Pending.
 * @version     $Id$
 */

/**
 * Creates FusionCharts XML
 * For more information on FusionCharts see http://www.fusioncharts.com.
 *
 * @category    Decimal
 * @package     Decimal_Chart
 * @subpackage  Adapter
 * @author      Rob Morgan <rm@decimal.com.au>
 * @author      Damien Byrne <db@decimal.com>
 * @copyright   Copyright (c) 2006-2015 Decimal Pty Ltd. (http://www.decimal.com.au)
 * @license     http://www.decimal.com.au    Proprietary. Patents Pending.
 */
class Decimal_Chart_Adapter_FusionCharts implements Decimal_Chart_Adapter_Interface {
    /**
     * @var Decimal_Chart
     */
    protected $chart;

    /**
     * The Chart XML Document
     *
     * @var DOMDocument
     */
    protected $xml;

    /**
     * Render a chart instance using the adapter.
     *
     * @param Decimal_Chart $chart
     * @return string XML
     */
    public function render(Decimal_Chart $chart) {
        if (!$this->xml) { // The XML was not generated yet
            $this->chart = $chart;
            $this->xml   = new DOMDocument('1.0', 'UTF-8');

            // Begin Chart Render
            $rootElement = $this->xml->createElement('chart');
            $this->xml->appendChild($rootElement);

            // Add Chart Properties
            $chartProperties = $this->chart->getProperties();
            foreach ($chartProperties as $propKey => $propValue) {
                $propAttr = $this->xml->createAttribute($propKey);
                $rootElement->appendChild($propAttr);

                $propText = $this->xml->createTextNode($propValue);
                $propAttr->appendChild($propText);
            }

            // Create Categories
            $categories = $this->xml->createElement('categories');
            $rootElement->appendChild($categories);

            foreach ($this->chart->getCategories() as $categoryKey) {
                $categoryElement = $this->xml->createElement('category');
                $categories->appendchild($categoryElement);

                $labelAttr = $this->xml->createAttribute('label');
                $categoryElement->appendChild($labelAttr);

                $labelText = $this->xml->createTextNode($categoryKey);
                $labelAttr->appendChild($labelText);
            }

            // Create Datasets
            /** @var Decimal_Chart_Dataset $dataset */
            foreach ($this->chart->getDatasets() as $dataset) {
                $datasetElement = $this->xml->createElement('dataset');
                $rootElement->appendChild($datasetElement);

                // Add in Dataset Properties
                $datasetProperties = $dataset->getProperties();
                foreach ($datasetProperties as $propKey => $propValue) {
                    $propAttr = $this->xml->createAttribute($propKey);
                    $datasetElement->appendChild($propAttr);

                    $propText = $this->xml->createTextNode($propValue);
                    $propAttr->appendChild($propText);
                }

                // Add Dataset Values
                $datasetValues = $dataset->getData();
                foreach ($datasetValues as $value) {
                    $setElement = $this->xml->createElement('set');
                    $datasetElement->appendChild($setElement);

                    $valueAttr = $this->xml->createAttribute('value');
                    $setElement->appendChild($valueAttr);

                    $valueText = $this->xml->createTextNode($value);
                    $valueAttr->appendChild($valueText);
                }
            }
        }

        return $this->xml->saveXML();
    }

    /**
     * Returns the full javascript
     *
     * @return string
     * @throws Decimal_Chart_Exception
     */
    public function getChartJavascriptFunction() {
        throw new Exception("Not implemented");
    }

    /**
     * @return string
     * @throws Decimal_Chart_Exception
     */
    public function getJavascriptChartLibrary() {
        // TODO: move the locations to the application.ini
        $library = file_get_contents('C:\inetpub\wwwroot\dcwealth\src\public\common\components\vendors\fusioncharts-xt\js\fusioncharts.js');
        $library .= file_get_contents('C:\inetpub\wwwroot\dcwealth\src\public\common\components\vendors\fusioncharts-xt\js\fusioncharts.charts.js');

        return $library;
    }


    /**
     * Sets the chart instance for the adapter.
     *
     * @param Decimal_Chart $chart
     * @return Decimal_Chart_Adapter_Interface
     */
    public function setChart(Decimal_Chart $chart) {
        $this->chart = $chart;

        return $this;
    }
        
    /** @inheritdoc */
    public function getChartTheme() {
        throw new Exception("Not implemented");
    }
}
