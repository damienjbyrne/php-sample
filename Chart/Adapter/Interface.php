<?php

/**
 * Decimal Framework
 *
 * LICENSE
 *
 * Please do not distribute this software.
 *
 * @category    Decimal
 * @package     Decimal_Chart
 * @copyright   Copyright (c) 2006-2015 Decimal Pty Ltd. (http://www.decimal.com.au)
 * @license     http://www.decimal.com.au    Proprietary. Patents Pending.
 * @version     $Id$
 */

/**
 * Chart Adapter Interface.
 *
 * @category    Decimal
 * @package     Decimal_Chart
 * @subpackage  Adapter
 * @author      Rob Morgan <rm@decimal.com.au>
 * @author      Damien Byrne <db@decimal.com>
 * @copyright   Copyright (c) 2006-2015 Decimal Pty Ltd. (http://www.decimal.com.au)
 * @license     http://www.decimal.com.au    Proprietary. Patents Pending.
 */
interface Decimal_Chart_Adapter_Interface
{
    /**
     * Sets the chart instance for the adapter.
     *
     * @param Decimal_Chart $chart
     * @return Decimal_Chart_Adapter_Interface
     */
    public function setChart(Decimal_Chart $chart);

    /**
     * Render a chart instance using the adapter.
     * 
     * @param Decimal_Chart $chart
     * @return string
     */
    public function render(Decimal_Chart $chart);

    /**
     * Returns the full javascript
     *
     * @return string
     * @throws Decimal_Chart_Exception
     */
    public function getChartJavascriptFunction();

    /**
     * @return string
     * @throws Decimal_Chart_Exception
     */
    public function getJavascriptChartLibrary();


    /**
     * @return string
     * @throws Decimal_Chart_Exception
     */
    public function getChartTheme();

}
