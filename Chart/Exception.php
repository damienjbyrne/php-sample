<?php

/**
 * Decimal Framework
 * 
 * LICENSE
 * 
 * Please do not distribute this software.
 * 
 * @category    Decimal
 * @package     Decimal_Chart
 * @copyright   Copyright (c) 2006-2015 Decimal Pty Ltd. (http://www.decimal.com.au)
 * @license     http://www.decimal.com.au    Proprietary. Patents Pending.
 */

/**
 * Decimal Chart Exception
 *
 * @category    Decimal
 * @package     Decimal_Chart
 * @author      Damien Byrne <db@decimal.com>
 * @copyright   Copyright (c) 2015 Decimal Pty Ltd. (http://www.decimal.com.au)
 * @license     http://www.decimal.com.au    Proprietary. Patents Pending.
 */
class Decimal_Chart_Exception extends Decimal_Exception {
}
