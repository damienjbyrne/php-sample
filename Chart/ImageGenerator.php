<?php
/**
 * Created by PhpStorm.
 * User: Damien J Byrne
 * Date: 5/10/2015
 * Time: 1:56 PM
 */

/**
 * Decimal Framework
 *
 * LICENSE
 *
 * Please do not distribute this software.
 *
 * @category    Decimal
 * @package     Decimal_Chart
 * @copyright   Copyright (c) 2006-2015 Decimal Pty Ltd. (http://www.decimal.com.au)
 * @license     http://www.decimal.com.au    Proprietary. Patents Pending.
 * @version     $Id$
 */

/**
 * Decimal Chart ImageGenerator
 *
 * This class takes a chart object and accesses a javascript chart generating function and javascript charting
 * library and uses them to render a chart image with WKHTMLTOIMAGE.
 *
 * LICENSE
 *
 * Please do not distribute this software.
 *
 * @category   Decimal
 * @package    Decimal_Model_Product
 * @subpackage ReportChart
 * @author     Damien Byrne <db@decimal.com>
 * @copyright  Copyright (c) 2015 Decimal Pty Ltd. (http://www.decimal.com)
 * @license    http://www.decimal.com  Proprietary. Patents Pending.
 * @version    $Id$
 */

class Decimal_Chart_ImageGenerator {

    /** @var  Decimal_Chart */
    protected $chart;

    /** @var  array options for converting */
    protected $renderOptions;

    /** @var string holds any error conditions generated by the image conversion */
    protected $err;

    const RENDER_MULTIPLIER = 3;

    public function __construct(Decimal_Chart $chart = null) {
        if ($chart !== null) {
            $this->chart = $chart;
        }
    }

    /**
     * @throws \Decimal_Exception
     */
    public function render() {
        // check that all the required data elements are present
        if ($this->isReady() === false) {
            throw new \Decimal_Exception("Chart render attempted with incorrect values set");
        }

        $config = \Zend_Registry::get('config');

        // build the arguments to pass to wkhtmltoimage
        // fixed arguments
        $args = " --quiet";
        $args .= " --format jpeg";
        $args .= " --zoom " . self::RENDER_MULTIPLIER;
        $args .= " --javascript-delay 0"; // make sure animation is turned off in the chart settings
        $this->chart->setAnimated(false);

        // configurable arguments
        $args .= " --crop-w " . (($this->chart->getWidth() * self::RENDER_MULTIPLIER) - 1);
        $args .= " --crop-h " . (($this->chart->getHeight() * self::RENDER_MULTIPLIER) - 1);

        // this sets the jpeg quality
        if (isset($this->renderOptions['quality'])) {
            $args = $args . " --quality {$this->renderOptions['quality']}";
        }

        // create the command string
        $executable = APPLICATION_PATH . $config->charts->wkhtmltoimage->exe;
        if(! file_exists($executable)){
            throw new Decimal_Chart_Exception("Cannot find wkhtmltoimage library.  Using path [$executable]");
        }
        $command    = "$executable $args - -";

        // get javascript chart library
        $chartLibrary = $this->chart->getJavascriptChartLibrary();
        $chartTheme   = $this->chart->getThemeContents();
        $chartJs      = $this->chart->getChartJavascriptFunction();

        // build the temp input string
        $inputStream = <<<HTML
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <style type="text/css">
            body{
                padding:0 0 0 0;
                margin:0 0 0 0;
            }
        </style>
        <script type="text/javascript">
            $chartLibrary
            $chartTheme
        </script>
    </head>
    <body>
        <div id="chartContainer"><small>Loading chart...</small></div>
    </body>
    <script>
        $chartJs
    </script>
</html>
HTML;

        //$date = new DateTime();
        //$ts   = $date->getTimestamp();
        //file_put_contents("c:\\temp\\chartDebug$ts.html", $inputStream);

        // execute the render
        $pipesDescription = [
            0 => ["pipe", "r"], // stdin is a pipe that the child will read from
            1 => ["pipe", "w"], // stdout is a pipe that the child will write output to
            2 => ["pipe", "w"]  // stderr is a pipe that the child will write errors to
        ];
        $wkHtmlProcess    = proc_open($command, $pipesDescription, $pipes);

        if (!is_resource($wkHtmlProcess)) {
            throw new \Decimal_Exception("An error took place while trying to open wkhtmltopdf");
        }

        fwrite($pipes[0], $inputStream);
        if (!fclose($pipes[0])) {
            throw new \Decimal_Exception("wkhtmltopdf write pipe failed to close");
        }

        $chartAsString = stream_get_contents($pipes[1]);
        if (!fclose($pipes[1])) {
            throw new \Decimal_Exception("wkhtmltopdf read pipe failed to close");
        }

        $errors = stream_get_contents($pipes[2]);
        if (!fclose($pipes[2])) {
            throw new \Decimal_Exception("wkhtmltopdf error pipe failed to close");
        }

        if (empty($errors) === false) {
            $this->err = $errors;

            return false;
        }

        // return stream and exit
        return $chartAsString;
    }

    /**
     * @return Decimal_Chart
     */
    public function getChart() {
        return $this->chart;
    }

    /**
     * @param Decimal_Chart $chart
     * @return Decimal_Chart_ImageGenerator
     */
    public function setChart($chart) {
        $this->chart = $chart;

        return $this;
    }

    /**
     * @param array $renderOptions
     * @return Decimal_Chart_ImageGenerator
     */
    public function setRenderOptions($renderOptions) {
        $this->renderOptions = $renderOptions;

        return $this;
    }

    /**
     * @return boolean check for success
     */
    public function getSuccess() {
        return empty($this->err);
    }

    /**
     * @return string check for the error conditions
     */
    public function getErr() {
        return $this->err;
    }

    /**
     * Checks to see if there is a chart set
     * @return bool
     */
    protected function isReady() {
        if ($this->chart === null) {
            return false;
        }

        return true;
    }

}
