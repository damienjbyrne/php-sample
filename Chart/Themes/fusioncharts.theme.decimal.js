/**
 * Decimal theme for FusionCharts
 * For more information on FusionCharts see http://www.fusioncharts.com.
 *
 * @category    Decimal
 * @package     Decimal_Chart
 * @subpackage  Themes
 * @author      Damien Byrne <db@decimal.com>
 * @copyright   Copyright (c) 2006-2015 Decimal Pty Ltd. (http://www.decimal.com)
 * @license     http://www.decimal.com    Proprietary. Patents Pending.
 */


//The `FusionCharts.register()` API is used to register the new theme in the FusionCharts core.
FusionCharts.register('theme', {
    name: 'decimal',
    theme: {
        base: {
            chart: {
                animation: '0',
                paletteColors: '#E48701,#A5BC4E,#1B95D9, #CACA9E, #6693B0, #F05E27, #86D1E4, #E4F9A0, #FFD512, #75B000, #0662B0, #EDE8C6, #CC3300, #D1DFE7, #52D4CA, #C5E05D, #E7C174, #FFF797, #C5F68F, #BDF1E6, #9E987D, #EB988D, #91C9E5, #93DC4A, #FFB900, #9EBBCD, #009797, #0DB2C2',
                caption: '',
                subCaption: '',
                captionAlignment: "center",
                captionOnTop: "1",
                captionHorizontalPadding: "10", // for non-centre aligned captions in pixels
                captionFont: 'Arial',
                captionFontSize: '12',
                captionFontBold: '1',
                subCaptionFont: 'Arial',
                subCaptionFontSize: '10',
                subCaptionFontBold: '0',
                bgColor: '#FFFFFF',
                showValues: '0',
                xAxisNameFont: "Arial",
                xAxisNameFontSize: "11",
                xAxisNameFontBold: "0",
                yAxisNameFont: "Arial",
                yAxisNameFontBold: "0",
                formatNumber: '1',
                formatNumberScale: '0',
                showHoverEffect: '0',
                placeValuesInside: '0',
                labelDisplay: "stagger",
                baseFontColor: "#333333",
                baseFont: "Arial, San Serif",
                showBorder: "0",
                showShadow: "0",
                canvasBgColor: "#ffffff",
                showCanvasBorder: "1",
                canvasBorderThickness: "1",
                useplotgradientcolor: "0",
                useRoundEdges: "0",
                showPlotBorder: "0",
                showAlternateHGridColor: "0",
                showAlternateVGridColor: "0",
                toolTipColor: "#ffffff",
                toolTipBorderThickness: "0",
                toolTipBgColor: "#000000",
                toolTipBgAlpha: "80",
                toolTipBorderRadius: "2",
                toolTipPadding: "5",
                legendBgAlpha: "0",
                legendBorderAlpha: "0",
                legendShadow: "0",
                legendItemFontSize: "10",
                legendItemFontColor: "#666666",
                legendCaptionFontSize: "9",
                legendPosition: "bottom", // only options are bottom and right ??
                divlineAlpha: "100",
                divlineColor: "#999999",
                divlineThickness: "1",
                divLineIsDashed: "1",
                divLineDashLen: "1",
                divLineGapLen: "1",
                scrollheight: "10",
                flatScrollBars: "1",
                scrollShowButtons: "0",
                scrollColor: "#cccccc",
                valueFontSize: "10",
                showXAxisLine: "1",
                xAxisLineThickness: "1",
                xAxisLineColor: "#999999",
                drawAnchors: "0",
                numDivLines: "6"
            },
            dataset: [
                {}
            ],
            trendlines: [{
                color: '#FF000',
                thickness: '3',
                dashed: '1',
                dashLen: '4',
                dashGap: '2'
            }]
        },
        line: {
            chart: {
                showPercentInToolTip: '1',
                enableSmartLabels: '1',
                lineThickness: "2"
            }
        },
        column2d: {
            chart: {
                showPercentInToolTip: '1',
                enableSmartLabels: '1',
                valueFontColor: "#ffffff",
                rotateValues: "1"
            }
        },
        column3d: {
            chart: {
                showPercentInToolTip: '1',
                enableSmartLabels: '1',
                valueFontColor: "#ffffff",
                rotateValues: "1"
            }
        },
        stackedColumn2d: {
            chart: {
                showPercentInToolTip: '1',
                enableSmartLabels: '1'
            }
        },
        stackedColumn3d: {
            chart: {
                showPercentInToolTip: '1',
                enableSmartLabels: '1'
            }
        },
        pie2d: {
            chart: {
                placeValuesInside: "0",
                showPercentValues: "1",
                showLabels: "0",
                showValues: "1",
                showZeroPies: "0",
                enableSmartLabels: '1',
                captionPadding: "15",
                showLegend: "1",
                legendBorderAlpha: "70",
                decimals: "2"
            }
        },
        pie3d: {
            chart: {
                use3DLighting: "0",
                showShadow: "0",
                pieYScale: "60",
                placeValuesInside: "0",
                showPercentValues: "1",
                showLabels: "0",
                showValues: "1",
                showZeroPies: "0",
                enableSmartLabels: '1',
                captionPadding: "15",
                startingAngle: "180",
                showLegend: "1",
                legendBorderAlpha: "70",
                decimals: "2"
            }
        },
        stepchart: {
            chart: {
                anchorMinRenderDistance: '20'
            }
        },
        geo: {
            chart: {
                showLabels: "0",
                fillColor: "#0075c2",
                showBorder: "1",
                borderColor: "#eeeeee",
                borderThickness: "1",
                borderAlpha: "50",
                entityFillhoverColor: "#0075c2",
                entityFillhoverAlpha: "80",
                connectorColor: "#cccccc",
                connectorThickness: "1.5",
                markerFillHoverAlpha: "90"
            }
        },
        doughnut2d: {
            chart: {
                placeValuesInside: "0",
                use3dlighting: "0",
                valueFontColor: "#333333",
                centerLabelFontSize: "12",
                centerLabelBold: "1",
                centerLabelFontColor: "#333333",
                captionPadding: "15"
            }
        },
        spline: {
            chart: {
                lineThickness: "2"
            }
        },
        bar2d: {
            chart: {
                valueFontColor: "#ffffff"
            }
        },
        bar3d: {
            chart: {
                valueFontColor: "#ffffff"
            }
        },
        area2d: {
            chart: {
                valueBgColor: "#ffffff",
                valueBgAlpha: "90",
                valueBorderPadding: "-2",
                valueBorderRadius: "2"
            }
        },
        splinearea: {
            chart: {
                valueBgColor: "#ffffff",
                valueBgAlpha: "90",
                valueBorderPadding: "-2",
                valueBorderRadius: "2"
            }
        },
        mscolumn2d: {
            chart: {
                valueFontColor: "#ffffff",
                rotateValues: "1"
            }
        },
        mscolumn3d: {
            chart: {
                showValues: "0"
            }
        },
        msstackedcolumn2dlinedy: {
            chart: {
                showValues: "0"
            }
        },
        stackedcolumn2d: {
            chart: {
                showValues: "0"
            }
        },
        stackedarea2d: {
            chart: {
                valueBgColor: "#ffffff",
                valueBgAlpha: "90",
                valueBorderPadding: "-2",
                valueBorderRadius: "2"
            }
        },
        stackedbar2d: {
            chart: {
                showValues: "0"
            }
        },
        msstackedcolumn2d: {
            chart: {
                showValues: "0"
            }
        },
        mscombi3d: {
            chart: {
                showValues: "0"
            }
        },
        mscombi2d: {
            chart: {
                showValues: "0"
            }
        },
        mscolumn3dlinedy: {
            chart: {
                showValues: "0"
            }
        },
        stackedcolumn3dline: {
            chart: {
                showValues: "0"
            }
        },
        stackedcolumn2dline: {
            chart: {
                showValues: "0"
            }
        },
        scrollstackedcolumn2d: {
            chart: {
                valueFontColor: "#ffffff"
            }
        },
        scrollcombi2d: {
            chart: {
                showValues: "0"
            }
        },
        scrollcombidy2d: {
            chart: {
                showValues: "0"
            }
        },
        logstackedcolumn2d: {
            chart: {
                showValues: "0"
            }
        },
        logmsline: {
            chart: {
                showValues: "0"
            }
        },
        logmscolumn2d: {
            chart: {
                showValues: "0"
            }
        },
        msstackedcombidy2d: {
            chart: {
                showValues: "0"
            }
        },
        scrollcolumn2d: {
            chart: {
                valueFontColor: "#ffffff",
                rotateValues: "1"
            }
        },
        pareto2d: {
            chart: {
                paletteColors: "#0075c2",
                showValues: "0"
            }
        },
        pareto3d: {
            chart: {
                paletteColors: "#0075c2",
                showValues: "0"
            }
        },
        angulargauge: {
            chart: {
                pivotFillColor: "#ffffff",
                pivotRadius: "4",
                gaugeFillMix: "{light+0}",
                showTickValues: "1",
                majorTMHeight: "12",
                majorTMThickness: "2",
                majorTMColor: "#000000",
                minorTMNumber: "0",
                tickValueDistance: "10",
                valueFontSize: "24",
                valueFontBold: "1",
                gaugeInnerRadius: "50%",
                showHoverEffect: "0"
            },
            dials: {
                dial: [{
                    baseWidth: "10",
                    rearExtension: "7",
                    bgColor: "#000000",
                    bgAlpha: "100",
                    borderColor: "#666666",
                    bgHoverAlpha: "20"
                }
                ]
            }
        },
        hlineargauge: {
            chart: {
                pointerFillColor: "#ffffff",
                gaugeFillMix: "{light+0}",
                showTickValues: "1",
                majorTMHeight: "3",
                majorTMColor: "#000000",
                minorTMNumber: "0",
                valueFontSize: "18",
                valueFontBold: "1"
            },
            pointers: {
                pointer: [{}

                ]
            }
        },
        bubble: {
            chart: {
                use3dlighting: "0",
                showplotborder: "0",
                showYAxisLine: "1",
                yAxisLineThickness: "1",
                yAxisLineColor: "#999999",
                showAlternateHGridColor: "0",
                showAlternateVGridColor: "0",
                plotFillHoverColor: "#2b8ecf",
                drawQuadrant: '1',
                quadrantLineColor: '3',
                quadrantLineThickness: '1',
                quadrantLineAlpha: '4'
            },
            categories: [{
                verticalLineDashed: "1",
                verticalLineDashLen: "1",
                verticalLineDashGap: "1",
                verticalLineThickness: "1",
                verticalLineColor: "#000000",
                category: [{}

                ]
            }
            ],
            dataset: [{
                regressionLineColor: '#123456',
                regressionLineThickness: '3',
                regressionLineAlpha: '70'
            }],
            vtrendlines: [{
                line: [{
                    alpha: "0"
                }
                ]
            }
            ]
        },
        scatter: {
            chart: {
                use3dlighting: "0",
                showYAxisLine: "1",
                yAxisLineThickness: "1",
                yAxisLineColor: "#999999",
                showAlternateHGridColor: "0",
                showAlternateVGridColor: "0"
            },
            categories: [{
                verticalLineDashed: "1",
                verticalLineDashLen: "1",
                verticalLineDashGap: "1",
                verticalLineThickness: "1",
                verticalLineColor: "#000000",
                category: [{}

                ]
            }
            ],
            vtrendlines: [{
                line: [{
                    alpha: "0"
                }
                ]
            }
            ]
        },
        boxandwhisker2d: {
            chart: {
                valueBgColor: "#ffffff",
                valueBgAlpha: "90",
                valueBorderPadding: "-2",
                valueBorderRadius: "2"
            }
        },
        thermometer: {
            chart: {
                gaugeFillColor: "#0075c2"
            }
        },
        cylinder: {
            chart: {
                cylFillColor: "#0075c2"
            }
        },
        sparkline: {
            chart: {
                linecolor: "#0075c2"
            }
        },
        sparkcolumn: {
            chart: {
                plotFillColor: "#0075c2"
            }
        },
        sparkwinloss: {
            chart: {
                winColor: "#0075c2",
                lossColor: "#1aaf5d",
                drawColor: "#f2c500",
                scoreLessColor: "#f45b00"
            }
        },
        hbullet: {
            chart: {
                plotFillColor: "#0075c2",
                targetColor: "#1aaf5d"
            }
        },
        vbullet: {
            chart: {
                plotFillColor: "#0075c2",
                targetColor: "#1aaf5d"
            }
        }
    }
});
