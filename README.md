# README #

Sample code in PHP

### What is this repository for? ###

This is a repo of some sample code that I wrote while at one of my employers. I am sharing this with their permission to demonstrate some of my programming style.

### What does the code do? ###
The code is a heavy expansion of code that generated a dataset for a fusionchart javascript function and converted the php datastructure to XML.  I expanded it to:

 * create JSON data in addition to XML
 * generate a small dummy html page with the javascript references
 * pass the html page through wkhtmltoimage.exe to generate a jpeg
 * receive the bit stream back from wkhtmltoimage.exe and 64bit encode it
 * pass this encoded string back to calling code

### Other notes ###
This code is not quite structured the way I would like it to be.  This is a legacy from the previous format of the code and the requirement to allow the old code to keep working.  In particular the chart has a reference to the adapter and the adapter holds a reference back to the chart.  This needs to be decoupled.